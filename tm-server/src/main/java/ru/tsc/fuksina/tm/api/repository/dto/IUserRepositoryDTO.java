package ru.tsc.fuksina.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.fuksina.tm.dto.model.UserDTO;

public interface IUserRepositoryDTO extends IRepositoryDTO<UserDTO> {

    @Nullable
    UserDTO findOneByLogin(@NotNull String login);

    @Nullable
    UserDTO findOneByEmail(@NotNull String email);

    void removeByLogin(@NotNull String login);

}
